/* A mining bot for metin2 coded by Patryk Seregiet somewhere in 2015
How does it work ? There are ores in random places on the map, they spawn
every couple of minutes. If you have a pickaxe, you can click on it and
your character will start digging. If you're lucky you will get an item
after 10-30 seconds and you can try again, untill the ore dissapears.
I've discovered something interesting about this though. All you need is the
VID of the ore and just send the Click packet with it and it will start digging
regardless of where you are ! So this bot sits in a corner of the map and just sends
OnClick packets with random VIDs untill it is an ore, then it keeps digging, collects 
the stuff and tries another one. This hack uses hardcoded offsets and addresses, so 
it won't work anymore. I did it in such a way, because this particular EXE was used
on almost all private servers at that time. The official server already encrypted their
exe and other stuff like that, I wasn't able to crack it....

THIS CODE ENTERED PUBLIC DOMAIN IN 2017 !
*/

#include "stdafx.h"
#include "hooks.h"



//*********************** GLOBALS
//*******************************
//*******************************

char gGamePhase=false;
char gSelectPhase=false;

int gLoopTime=0;
int gStartVid=0;
int gPushDig=0;
int gOreVid=0;
char gFailDigMSG[100];
char gSuccessDigMSG[100];
char gPickUpMSG[100];
bool gDigingNow = false;
bool gDigCanceled = false;
const char* gDigCanceledMSG = "Wydobycie A";
const char* gOreIsGoneMSG = "Nie ma";
const char* gDigCompleteMSG = "Wydobycie z";
const char* gDigMorePointsMSG = "Twoje P";

int current_vid = 0;
bool GameLoop = false;
DWORD GameLoopTime;
bool gFoundOre = false;

int first_vid=0;
int min_vid =0;
int max_vid =0;

DWORD gTheVid=0;


bool gDigComplete = false;
std::vector <DWORD> OresVidVector;

DWORD gTotalTicks=0;
bool gAfterScanStuff=false;

int gStartDiging4_arg1=0;
int gStartDiging4_step=0;


int gWaitForDig2_step=0;
int gWaitForDig2_arg1=0;


DWORD StartDiging4();
DWORD WaitForDig();
DWORD CancelDiging2();
DWORD WaitForDig2();
DWORD CancelDigingWithoutConfirmation();

void LoadConfig()
{
	FILE* f = fopen("d:\\mining.cfg", "r");
	if (!f)
	{
		printf("error while loading config\n");
		system("PAUSE");
		return;
	}

	char line[100];
	int c=0;
	while (!feof(f))
	{
		fgets(line, 100, f);
		switch (c)
		{
		case 0:
			gLoopTime = atoi(line);
			break;
		case 1:
			gStartVid = atoi(line);
			break;
		case 2:
			strcpy(gFailDigMSG, line);
			break;
		case 3:
			strcpy(gSuccessDigMSG, line);
			break;
		case 4:
			strcpy(gPickUpMSG, line);
			break;
		default:
			printf("CONFIG : more than 4 lines !?\n");
			break;
		}
		c++;
	}
	fclose(f);
	printf("Config loaded :)\n");
}

int gCancelDiging_step=0;
int gCancelDigingWC_step=0;


#define SendSelectCharacterPacket(idx) ((void(__fastcall*)(DWORD,DWORD,BYTE))0x4208F0)(*(DWORD*)(0x61C028 ),0,idx)

DWORD SeekOres()
{
	DWORD ret_ticks = 1;
	if (!current_vid)
	{
		current_vid = min_vid;
	}
	SendCLICKPacket(current_vid);
	printf("%d\r", current_vid);
	current_vid++;
	if (current_vid >= max_vid)
	{
		printf("max_vid reached\n");
		ret_ticks =0;
	}
	if (gPushDig && gOreVid)
	{
		printf("found vid = %d cycles = %d\n", gOreVid, gPushDig);
		current_vid = gOreVid+1;
		gFoundOre = true;
		ret_ticks = 0;
	}
	return ret_ticks;
}
char CancelDigDirection=0;
void CancelDig()
{
	bool movement[4] = {0,0,0,0};
	while (!gDigCanceled)
	{
		/*moving for 10ms just to cancel diging*/
		movement[CancelDigDirection]=1;
		CancelDigDirection++;
		if (CancelDigDirection >3)
		{
			CancelDigDirection=0;
		}
		DWORD PythonPlayerInstance = *(DWORD*)(0x61997C);
		((void(__fastcall*)(DWORD,DWORD,bool,bool,bool,bool))0x0043DA40)(PythonPlayerInstance,0,
			movement[0],movement[1],movement[2],movement[3]);
		Sleep(100);
		((void(__fastcall*)(DWORD,DWORD,bool,bool,bool,bool))0x0043DA40)(PythonPlayerInstance,0,0,0,0,0);
		Sleep(100);
		//Teleport(1000, 1000);
	}
	printf("CANCEL DIG FINISH\n\n");
	gDigCanceled=false;
	Sleep(250);
	
}

void StartDigTEST(DWORD v)
{
	// Cancel Dig 
	DWORD PythonPlayerInstance = *(DWORD*)(0x61997C);
	((void(__fastcall*)(DWORD,DWORD,bool,bool,bool,bool))0x0043DA40)(PythonPlayerInstance,0,0,1,0,0);
	Sleep(50);
	((void(__fastcall*)(DWORD,DWORD,bool,bool,bool,bool))0x0043DA40)(PythonPlayerInstance,0,0,0,0,0);
	gPushDig=0;
	gOreVid=0;
	Sleep(10);
	
	bool sendPacket=true;
	int c =0;
	int d =0;
	while (1)
	{
		if (sendPacket)
		{
			printf("SEND CLICK PACKET ( %d )\n", v);
			SendCLICKPacket(v);
			sendPacket=false;
		}
		
		if (!gPushDig)
		{
			Sleep(50);
			c++;
			if (c>=30)
			{
				printf("FAIL :( \n");
				c=0;
				d++;
				if (d>=10)
				{
					printf("MEGA FAIL :(\n");
					d=0;
					current_vid = v+1;
					gPushDig=0;
					gOreVid=0;
					sendPacket=true;
					gDigComplete=false;
					gDigCanceled=false;
					return;
				}
				sendPacket=true;
				continue;
			}
		}
		else
		{
			d=0;
			printf("gPushDig = %d\n", gPushDig);
			c=0;
			
			if (gPushDig > 8)
			{
				printf("Cancel dig !\n");
				CancelDig();
				sendPacket=true;
				continue;
			}
			else
			{
				printf("**** MINING %d ****\n", v);
				gDigCanceled=false;
				gDigComplete=false;
				//int loading=-1;
				while (1)
				{
					if (gDigCanceled || gDigComplete)
					{
						break;
					}
					Sleep(10);
				}

				if (gDigCanceled)
				{
					printf("The dig has been canceled\n");
					gDigComplete=false;
					gDigCanceled=false;
					gPushDig=0;
					gOreVid=0;
					break;
				}
				
				if (gDigComplete)
				{
					printf("Dig has been done !\n Picking up the shit and starting again\n");
					
					for (int i =0;i<3;i++)
					{
						PickUpCloseItems_asm();
					}
					gPushDig=0;
					gOreVid=0;
					sendPacket=true;
					gDigComplete=false;
					gDigCanceled=false;
					continue;
				}
				printf("WTF???\n");
			}
		}
	}
}

extern "C" void _cdecl OnUpdateHook_c()
{
	if (gGamePhase && GameLoop)
	{
		if (!gTotalTicks)
		{
			gTotalTicks = GetTickCount();
		}
		DWORD tick = GetTickCount();
		if (gTotalTicks <= tick)
		{
			DWORD ret = SeekOres();
			if (!ret)
			{
				GameLoop=false;
				gTheVid = gOreVid;
				gAfterScanStuff=true;
			}
			gTotalTicks = tick + ret;
		}
	}
	/*if (gGamePhase && gAfterScanStuff)
	{
		if (!gTotalTicks)
		{
			gTotalTicks=GetTickCount();
		}
		DWORD tick = GetTickCount();
		if (gTotalTicks <= tick)
		{
			
			DWORD ret = StartDiging4();
			gTotalTicks = tick + ret;
		}
	}*/
}


extern "C" void __declspec( naked ) OnUpdateHook_asm() // 0x00535A13
{
	__asm
	{
		call dword ptr [eax+8]
		mov ecx, [esi + 0x6C]
		push ecx
		call OnUpdateHook_c
		pop ecx
		
		mov eax, 0x00535A19
		jmp eax
	}
}


DWORD WINAPI LetsStart(LPVOID lpParam)
{
	/*GameLoopTime = GetTickCount();
	LoadConfig();
    //CallPatch(0x00439CE6, (DWORD)&HOOK__SendClickPacket, 0);
	CallPatch(0x0041CA64, (DWORD)&RecvMiningCount_asm, 0);

	CallPatch(0x0041C701, (DWORD)&RecvChatPacket_asm_v2, 0);

	JMPPatch(0x0042B326, (DWORD)&CreateInstanceHook_asm, 0);

	JMPPatch(0x0041D134, (DWORD)&ONGamePhase, 0);
	JMPPatch(0x00420F63, (DWORD)&OnSelectPhase, 0);
	JMPPatch(0x00417FFF, (DWORD)&ONLeaveGamePhase,2);

	JMPPatch(0x00535A13, (DWORD)&OnUpdateHook_asm, 1);*/



	AllocConsole();
	freopen("CONOUT$", "w", stdout);
	freopen("CONIN$", "r", stdin);
	std::cout << "Mining bot by Patryk\n";

	system("PAUSE");
	GameLoop = true;
	
	while (1)
	{
		//if (gSelectPhase)
		//{
		//	printf("SendSelectChar\n");
		//	SendSelectCharacterPacket(1);
		//}
		if (!GameLoop && gAfterScanStuff)
		{
			StartDigTEST(gTheVid);
			gAfterScanStuff=false;
			GameLoop=true;

		}
		Sleep(100);
	}
	/*while (1)
	{
		if (gFoundOre)
		{
			printf("DEBUG : gFoundOre loop start\n");
			StartDiging3(gOreVid);
			printf("StartDiging2 has finish, but gFoundOre is still true\n Sleep(10k)\n");
			Sleep(10000);
		}
		Sleep(1000);
	}*/

	return 0;
}
void START_K()
{
	DWORD c=0;
	DWORD tID =0;
	CreateThread(0,0,LetsStart,&c,0,&tID);
}
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		START_K();
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

