#include "hooks.h"

extern const char* gOreIsGoneMSG;
extern const char* gDigCompleteMSG;
extern const char* gDigCanceledMSG;
extern const char* gDigMorePointsMSG;
extern bool gDigCanceled;
extern bool gDigComplete;
extern char gGamePhase;
extern char gSelectPhase;

extern int gOreVid;
extern int gPushDig;
extern int first_vid;
extern int min_vid;
extern int max_vid;

extern "C" void CallPatch(DWORD org, DWORD new_call, int NOPs)
{
//	mprotect((const void*)pSrc, 5 + nNops, 7);

	*(unsigned char*)(org) = 0xE8;
	*(unsigned int*)(org + 1) = new_call - org - 5;
	int i;
	for (i = 0; i < NOPs; ++i)
	{ 
		*(unsigned char*)(org + 5 + i) = 0x90;
	}
}
extern "C" void JMPPatch(DWORD org, DWORD new_call, int NOPs)
{
//	mprotect((const void*)pSrc, 5 + nNops, 7);

	*(unsigned char*)(org) = 0xE9;
	*(unsigned int*)(org + 1) = new_call - org - 5;
	int i;
	for (i = 0; i < NOPs; ++i)
	{ 
		*(unsigned char*)(org + 5 + i) = 0x90;
	}
}
void SendCLICKPacket(DWORD arg_v)
{
	DWORD SendPacketCall = 0x4169D0;
	DWORD SendPacketClass = *(DWORD*)(0x61C028);
	__asm
	{
		push arg_v
		mov ecx, SendPacketClass
		call SendPacketCall
	}
}
bool CompareStrings(char* a1, const char* a2)
{
	for (DWORD i = 0; i < strlen(a2); i++)
	{
		if ( a1[i] != a2[i])
		{
			return false;
		}
	}
	return true;
}

extern "C" DWORD _cdecl RecvChatPacket_c_v2(DWORD ecx)
{
	char chat[1025];
	chat_packet pChat;
	
	bool ret;
	ret = ((bool(__fastcall*)(DWORD,DWORD,DWORD,void*))0x4F32A0)(ecx,0,9, &pChat);
	
	int size = pChat.size - 9;

	ret = ((bool(__fastcall*)(DWORD,DWORD,DWORD,void*))0x4F32A0)(ecx,0,size, chat);

	chat[size] = 0;

	if (pChat.type == 1)
	{
		printf("%s\n",chat);
		if (CompareStrings(chat, gDigMorePointsMSG))
		{
			gDigComplete=true;
			return ecx;
		}
		bool DigCancel1 = CompareStrings(chat, gOreIsGoneMSG);
		bool DigCancel2 = CompareStrings(chat, gDigCanceledMSG);

		if (DigCancel1 || DigCancel2)
		{
			gDigCanceled=true;
			return ecx;
		}

		bool gDigComplete = CompareStrings(chat, gDigCompleteMSG);
		
		printf("%u gDigCanceled = %d, gDigComplete = %d\n", GetTickCount(), gDigCanceled, gDigComplete);
	}
	
	return ecx;
}
extern "C" void __declspec( naked ) RecvChatPacket_asm_v2()
{
	__asm
	{
		sub esp, 8
		push ecx
		call RecvChatPacket_c_v2
		add esp, 4

		mov ecx, eax
		add esp, 8
		mov eax, 1
		ret
	}
}

extern "C" void __declspec( naked ) RecvMiningCount_asm()
{
	//DWORD RecvPacket_i_guess = 0x4F32A0;
	__asm
	{
		sub esp, 12
		lea eax, [esp]
		push eax
		push 10
		mov eax, 0x4F32A0
		call eax
		test al, al
		jnz label_1

		add esp, 12
		ret

label_1:
		movzx ecx, byte ptr [esp + 9]
		mov gPushDig, ecx
		mov ecx, dword ptr [esp+5]
		mov gOreVid, ecx
		mov al, 1
		add esp, 12
		ret
	}
}

extern "C" void __declspec( naked ) PickUpCloseItems_asm() // same thing as pressing Z in game
{
	__asm
	{
		pushad
		mov eax, 0x61997c
		mov ecx, [eax]
		mov eax, 0x43ce80
		call eax
		popad
		ret
	}
}

extern "C" int _cdecl CreateInstanceHook_c(DWORD a1) // this is called when a new player/npc would be rendered
{
	/* the server sends the client packet with every player/npc (I call it actor/character)
	in some radius of the player. When the players moves around the map the server sends another
	packets and client deletes all the actors that are too far. This is a hook for a function
	that would add this actor to some array and render it on the map. I use it to find ores/players
	and because i don't render any actor besides players and ores the game works much faster.

	Also, by using this function and a function that can move the character across the whole map
	(which is done in this code (somewhere in backup.cpp) it's very easy to create an array of all
	actors on the map in just few seconds !

	*/
	int ret=0;
	DWORD v = *(DWORD*)(a1+20);
	DWORD r = *(DWORD*)(a1+24);
	if (r >= 20047 && r <=20059)
	{
		printf("\n\nfound ore ! %d %d\n\n", v, r);
		/*for (DWORD i = 0;i<OresVidVector.size();i++)
		{
			if (OresVidVector[i] == v)
			{
				printf("Ore already in the vector, skiping...\n");
				return 1;	
			}
		}
		OresVidVector.push_back(v);
		*/

		ret=1;
	}
	else if (r < 10)
	{
		printf("\nRendering Player !\n");
		if (!first_vid)
		{
			printf("base vid = %d\n", v);
			if (v > 30000)
			{
				min_vid = v - 3000;
				max_vid = v + 15000;
			}
			else
			{
				min_vid = v/2;
				max_vid = v+min_vid;
			}
			
			printf("min vid %d == max vid %d\n", min_vid,max_vid);
			first_vid = v;
		}
		
		ret=1;
	}
	return ret;
	
}

extern "C" void __declspec( naked ) CreateInstanceHook_asm()
{
	__asm
	{
		pushad
		push esi
		call CreateInstanceHook_c
		add esp, 4
		test eax, eax
		jz DontRenderLabel

		popad
		mov eax, [esi + 20]
		push edi
		push eax
		
		push 0x0042B32B
		ret
		// if the code jumps to this address
		// it will render the actor he found.
		// the jump below (0x0042B351 skips the rendering)
		// so the map will look empty, even though client
		// has received the packet about all the actors.
		// the function above (CreateInstanceHook_c) can 
		// put these actors to some array if needed.

DontRenderLabel:
	
		popad
		xor eax, eax
		push 0x0042B354
		ret
	}
}
extern "C" void __declspec( naked ) OnSelectPhase()
{
	__asm
	{
		mov gSelectPhase, 1
		push 0x5B71B0
		push 0x420F68
		ret
	}
}
extern "C" void __declspec( naked ) ONGamePhase()
{
	__asm
	{
		mov gGamePhase, 1
		mov gSelectPhase, 0
		push 0x5B6B38
		push 0x0041D139
		ret
	}
}
extern "C" void __declspec( naked ) ONLeaveGamePhase()
{
	__asm
	{
		mov gGamePhase, 0
		mov first_vid, 0
		mov byte ptr [esi + 0x7B3D],0
		push 0x00418006
		ret
	}
}
extern "C" void __declspec( naked ) SetPixelPosition(DWORD* pos)
{
	/* SetPixelPosition will put your character (or any actor, but this funcion has
	a fixed pointer to main character) on a certain coordinants on the map. It's basically 
	a teleporter funtion, the problem is, though, that the server checks if the character 
	moves to fast, so you can only jump by 20k units before the server will send you back
	(and leave some log about the hack for sure). It can be countered very easely,
	all you have to do is loop the SetPixelPosition with 10/20k jump in a loop with 
	10-50 ms cooldown. That basically makes a "slow" teleporter (which is still faster 
	than normal walking of course)
	*/
	__asm
	{
		push ebp
		mov ebp, esp
		push ebx
		push ecx // push ecx to preserve it
		sub esp, 20
		mov ebx, [ebp + 8]

		push ebx

		mov ecx, dword ptr [0x619980]
		mov ecx, [ecx + 16]
		
		mov eax, 0x0040C680
		call eax

		add esp, 20
		pop ecx
		pop ebx
		pop ebp
		ret
		
		// now we should have our character's instance in ecx
		// and the call bellow takes it as an argument through ecx

		


	}
}

void Teleport(float x, float y)
{
	//float poss[3] = {x,y,0};
	//SetPixelPosition((DWORD)poss);
	DWORD PythonPlayerInstance = *(DWORD*)(0x61997C);

	((void(__fastcall*)(DWORD,DWORD,bool,bool,bool,bool))0x0043DA40)(PythonPlayerInstance,0,0,0,0,0);

	DWORD Base = *(DWORD*)(*(DWORD*)(0x619980) +16) + 476;
	float pos[3] = {x,-y,0};
	((void(__fastcall*)(DWORD,DWORD,float*))0x4A3E10)(Base, 0,pos);

	
	((void(__fastcall*)(DWORD,DWORD,bool,bool,bool,bool))0x0043DA40)(PythonPlayerInstance,0,1,0,0,0);
}

/*
DWORD StartDiging3(DWORD v)
{
	printf("StartDiging3(%d)\n", v);

	CancelDigingWithoutConfirmation();
	gPushDig = 0;
	gOreVid = 0;
	int wait_counter=0;

	while (1)
	{
		SendCLICKPacket(v);
		
		if (!gPushDig)
		{
			Sleep(30);
			wait_counter++;
			if (wait_counter >= 10)
			{
				break;
			}
		}
		else
		{
			wait_counter=0;

			printf("gPushDig = %d\n", gPushDig);

			if (gPushDig > 5)
			{
				CancelDiging();
				continue;
			}
			else
			{
				printf(" MINING THE ORE ( %D ) \n ", gPushDig);

				WaitForDig();

				printf(" MINING COMPLETE\n");
			}
		}
	}
	printf("It seems, that this vid is not an ore\n");
	return 1;
}
DWORD StartDiging2(DWORD v)
{
	CancelDigingWithoutConfirmation();
	printf("StartDiging2(%d)\n", v);
	SendCLICKPacket(v);
	Sleep(250);
	
	int cc=0;
	int tt=0;
	while (1)
	{
		if (gOreVid && gPushDig)
		{
			if (gPushDig > 5)
			{
				printf("Cycles more than 5 (%d), let's try again\n", gPushDig);
				CancelDiging();
				Sleep(50);
				SendCLICKPacket(v);
				Sleep(250);
				continue;
			}
			printf("==== MINING ==== (vid = %d)\n", v);
			WaitForDig();
			
			// at this point, the ore was diged, but let's try to dig it again
			// till it dissapears

			SendCLICKPacket(v);
			Sleep(250);
		}
		
	}
	printf("It seems that this vid is not an ore (anymore?)\n Continue with the scan\n");
		GameLoop = true;
		gFoundOre = false;
		return 0;
	
}
void WaitForDig()
{
	printf("DEBUG : WaitForDig() start\n");
	gDigComplete=false;
	Sleep(10000); //the diging takes 10 seconds minimum
	while (!gDigComplete)
	{
		Sleep(10);
	}
	gDigComplete=false;
	for (int i = 0;i<3;i++)
	{
		PickUpCloseItems_asm();
		Sleep(150);
	}
	gPushDig=0;
	gOreVid=0;
	printf("WaitForDig finished\n");
}
*/