#ifndef __HOOKS
#define __HOOKS
#include <windows.h>
#include <stdio.h>
extern "C" void CallPatch(DWORD org, DWORD new_call, int NOPs);
extern "C" void JMPPatch(DWORD org, DWORD new_call, int NOPs);

void SendCLICKPacket(DWORD arg_v);
bool CompareStrings(char* a1, const char* a2);
extern "C" void  RecvChatPacket_asm_v2();
extern "C" void  RecvMiningCount_asm();
extern "C" void  PickUpCloseItems_asm();
extern "C" void  CreateInstanceHook_asm();
extern "C" void  ONGamePhase();
extern "C" void  ONLeaveGamePhase();
extern "C" void  OnSelectPhase();
void Teleport(float x, float y);


#pragma pack(1)
struct chat_packet
{
	BYTE header;
	WORD size;
	BYTE type;
	char unknown[5];
};
#pragma pack


// globals
//char gGamePhase=0;


#endif
