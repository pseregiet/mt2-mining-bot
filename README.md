## Mining Bot for Metin2

A mining bot for metin2 coded by Patryk Seregiet sometime in 2015.
How does it work ? There are ores in random places on the map, they spawn every couple of minutes. If you have a pickaxe, you can click on them and your character will start digging. If you're lucky you will get an item after 10-30 seconds and you can try again, untill the ore dissapears. I've discovered something interesting about this though. All you need is the VID of the ore and just send the Click packet with it and it will start digging regardless of where you are ! So this bot sits in a corner of the map and just sends OnClick packets with random VIDs untill it is an ore, then it keeps digging, collects the stuff and tries another one.

 This hack uses hardcoded offsets and addresses, so it won't work anymore. I did it in such a way, because this particular EXE was used on almost all private servers at that time. The official server already encrypted their exe, I wasn't able to crack it....

### How to compile ?
Just create an empty DLL project in any Visual Studio c++, put those files in the project and compile. Very easy.

### THIS CODE ENTERED PUBLIC DOMAIN IN 2017 !
Patryk Seregiet 2017